libdevel-declare-parser-perl (0.021-1) unstable; urgency=medium

  * Import upstream version 0.021.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Oct 2023 01:47:39 +0200

libdevel-declare-parser-perl (0.020-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-
    Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on libmodule-build-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 15:52:58 +0100

libdevel-declare-parser-perl (0.020-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 22:56:58 +0100

libdevel-declare-parser-perl (0.020-1) unstable; urgency=medium

  * Import upstream version 0.020.
  * Drop spelling.patch, merged upstream.
  * Update fix-pod.patch to match upstream changes in the file.
  * Update (build) dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 04 Jul 2015 19:20:43 +0200

libdevel-declare-parser-perl (0.17-2) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Add debian/upstream/metadata
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 3.9.6.
  * Drop version from libmodule-build-perl build dependency.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Mon, 01 Jun 2015 17:58:19 +0200

libdevel-declare-parser-perl (0.17-1) unstable; urgency=low

  * Initial release (closes: #696089).

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Dec 2012 17:42:55 +0100
